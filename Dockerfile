FROM ubuntu:bionic

# Update dependencies repository
RUN apt-get update

# Install dependencies
RUN apt-get -y install build-essential mysql-server-5.7 mysql-client-5.7 wget zip python3 python-pip curl git &&  \
  git config --global user.name "ign-fuelserver"  &&  \
  git config --global user.email "ign-fuelserver@test.org"

RUN pip install boto3==1.3.0

# Install Go 1.14.4
RUN curl -O https://dl.google.com/go/go1.14.4.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.14.4.linux-amd64.tar.gz
RUN chown -R root:root /usr/local/go
RUN rm go1.14.4.linux-amd64.tar.gz

# Configure Go
ENV GOPATH /go
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
ENV PATH ${GOPATH}/bin:$PATH
ENV PATH /usr/local/go/bin:$PATH


# Install golint
RUN echo go env GOPATH
RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.27.0
